//
//  ViewController.swift
//  CommonPaySDK
//
//  Created by yangpeng on 06/01/2023.
//  Copyright (c) 2023 yangpeng. All rights reserved.
//

import UIKit
import CommonPaySDK

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let iapButton = UIButton(frame: CGRectMake(20, 100, 80, 40))
        iapButton.setTitle("IAP", for: .normal)
        iapButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        iapButton.setTitleColor(UIColor.white, for: .normal)
        iapButton.backgroundColor = UIColor.purple
        iapButton.addTarget(self, action:#selector(iApClick), for: .touchUpInside)
        self.view.addSubview(iapButton)
        
        RMStore.default().addObserverSuccess { transaction in
            if let receiptUrl = Bundle.main.appStoreReceiptURL {
                //获取收据地
                let receiptData = NSData(contentsOf: receiptUrl)
                let receiptStr = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                print("receiptStr = \(String(describing: receiptStr))")
            }
            RMStore.default().verifyFinish(transaction);
            print("storePaymentTransactionFinished =",transaction?.payment.applicationUsername ?? "",transaction?.transactionIdentifier ?? "")
        } failure: { transaction, error in
            print("storePaymentTransactionFailed")
        }

    }
    
    @objc func iApClick(){
        
        if (RMStore.canMakePayments()) {
            let productsList: Set<String> = ["com.mugen.odyssey_iap_appstore_4.99_gem"]
            RMStore.default().requestProducts(productsList, success:{ products, invalidIds in
                let product:SKProduct = products![0] as! SKProduct
                print("success:",products ?? "",invalidIds ?? "")
                if (product.productIdentifier.count > 0) {
                    print("pay productIdentifier =",product.productIdentifier)
                    RMStore.default().addPayment(product.productIdentifier, user: "1234567890",  success: { transaction in
                        print("pay success = ",transaction ?? "")
                        //获得收据
                        if let receiptUrl = Bundle.main.appStoreReceiptURL {
                            //获取收据地
                            let receiptData = NSData(contentsOf: receiptUrl)
                            let receiptStr = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                            print("receiptStr = \(String(describing: receiptStr))")
                        }
                        RMStore.default().verifyFinish(transaction);
                    }, failure: { transaction, error in
                        print("pay fail = ",error?.localizedDescription ?? "")
                    })
                }
                
            } , failure: { error in
                print("fail：\(error!.localizedDescription)")
            })
        } else {
            print("不支持程序内付费")
        }
    }
    
//    func storePaymentTransactionFinished(_ notification: Notification!) {
//        let transaction:SKPaymentTransaction = notification.userInfo?["transaction"] as! SKPaymentTransaction
//        if let receiptUrl = Bundle.main.appStoreReceiptURL {
//            //获取收据地
//            let receiptData = NSData(contentsOf: receiptUrl)
//            let receiptStr = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//            print("notification receiptStr = \(String(describing: receiptStr))")
//        }
//        print("storePaymentTransactionFinished =",transaction.payment.applicationUsername ?? "",transaction.transactionIdentifier ?? "")
//    }
//    
//    func storePaymentTransactionFailed(_ notification: Notification!) {
//        print("storePaymentTransactionFailed")
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

